import { CalanderComponent } from './views/admin/calander/calander.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminlayoutComponent } from './layouts/adminlayout/adminlayout.component';
import { ProfillayoutComponent } from './layouts/profillayout/profillayout/profillayout.component';
import { UserlayoutComponent } from './layouts/userlayout/userlayout.component';

const routes: Routes = [
  {path:'',component:UserlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/user/home/home.module').then(m=>m.HomeModule)},
    {path:'login',loadChildren:()=>import('./views/user/signin/signin.module').then(m=>m.SigninModule)},
    {path:'register',loadChildren:()=>import('./views/user/signup/signup.module').then(m=>m.SignupModule)},
    {path:'espace',loadChildren:()=>import('./views/user/espace/espace.module').then(m=>m.EspaceModule)},
    {path:'evenement', loadChildren:()=>import('./views/user/evenement/evenement.module').then(m=>m.EvenementModule)},
    {path:'activite', loadChildren:()=>import('./views/user/activite/activite.module').then(m=>m.ActiviteModule)},
    {path:'tarif', loadChildren:()=>import('./views/user/tarif/tarif.module').then(m=>m.TarifModule)},
    {path:'calander', loadChildren:()=>import('./views/user/calander/calander.module').then(m=>m.CalanderModule)},







  ]},
  {path:'admin',component:AdminlayoutComponent,children:[
    {path:'',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'dashboard',loadChildren:()=>import('./views/admin/dashboard/dashboard.module').then(m=>m.DashboardModule)},
    {path:'adminlogin',loadChildren:()=>import('./views/admin/adminlogin/adminlogin.module').then(m=>m.AdminloginModule)},
    {path:'editact',loadChildren:()=>import('./views/admin/activite/editact/editact/editact.module').then(m=>m.EditactModule)},
    {path:'listeact',loadChildren:()=>import('./views/admin/activite/listeact/listeact/listeact.module').then(m=>m.ListeactModule)},
    {path:'addact',loadChildren:()=>import('./views/admin/activite/addactivite/addact/addact.module').then(m=>m.AddactModule)},
    {path:'addevents',loadChildren:()=>import('./views/admin/evenement/addevents/addevents/addevents.module').then(m=>m.AddeventsModule)},
    {path:'list',loadChildren:()=>import('./views/admin/evenement/listevents/list/list.module').then(m=>m.ListModule)},
    {path:'editevents',loadChildren:()=>import('./views/admin/evenement/editevents/editevents/editevents.module').then(m=>m.EditeventsModule)},
    {path:'editevents',loadChildren:()=>import('./views/admin/evenement/editevents/editevents/editevents.module').then(m=>m.EditeventsModule)},
    {path:'addespace',loadChildren:()=>import('./views/admin/espace/addespace/addespace.module').then(m=>m.AddespaceModule)},
    {path:'editespace',loadChildren:()=>import('./views/admin/espace/editespace/editespace.module').then(m=>m.EditespaceModule)},
    {path:'listespace',loadChildren:()=>import('./views/admin/espace/listespace/listespace.module').then(m=>m.ListespaceModule)},
    {path:'adduser',loadChildren:()=>import('./views/admin/utilisateur/adduser/adduser.module').then(m=>m.AdduserModule)},
    {path:'edituser',loadChildren:()=>import('./views/admin/utilisateur/edituser/edituser.module').then(m=>m.EdituserModule)},
    {path:'listeuser',loadChildren:()=>import('./views/admin/utilisateur/listeuser/listeuser.module').then(m=>m.ListeuserModule)},
    {path:'addtarif',loadChildren:()=>import('./views/admin/tarif/addtarif/addtarif.module').then(m=>m.AddtarifModule)},
    {path:'edittarif',loadChildren:()=>import('./views/admin/tarif/edittarif/edittarif.module').then(m=>m.EdittarifModule)},
    {path:'listetarif',loadChildren:()=>import('./views/admin/tarif/listetarif/listetarif.module').then(m=>m.ListetarifModule)},
    {path:'addcoach',loadChildren:()=>import('./views/admin/coach/addcoach/addcoach.module').then(m=>m.AddcoachModule)},
    {path:'listcoach',loadChildren:()=>import('./views/admin/coach/listcoach/listcoach.module').then(m=>m.ListcoachModule)},
    {path:'editcoach',loadChildren:()=>import('./views/admin/coach/editcoach/editcoach.module').then(m=>m.EditcoachModule)},
     {path:'calander',loadChildren:()=>import('./views/admin/calander/calander.module').then(m=>m.CalanderModule)},











  ]},

  {path:'profil',component:ProfillayoutComponent,children:[
    {path:'events',loadChildren:()=>import('./views/profiluser/events/events/events.module').then(m=>m.EventsModule)},
    {path:'monprofil',loadChildren:()=>import('./views/profiluser/monprofil/monprofil/monprofil.module').then(m=>m.MonprofilModule)},
    {path:'editprofil',loadChildren:()=>import('./views/profiluser/monprofil/editmonprofil/editmonprofil.module').then(m=>m.EditmonprofilModule)},
    {path:'list',loadChildren:()=>import('./views/profiluser/listabonne/listabonne.module').then(m=>m.ListabonneModule)},




  ]},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
