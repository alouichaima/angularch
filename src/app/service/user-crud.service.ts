import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { utilisateur } from '../models/utilisateur.model';

@Injectable({
  providedIn: 'root'
})
export class UserCRUDService {

  private baseUrl = 'http://localhost:8087/utilisateur';

  constructor(private http: HttpClient) { }
  getAll()
  {
    return this.http.get(this.baseUrl + '/all');
  }

  adduser(u:utilisateur):Observable<object>{
   return this.http.post("http://localhost:8087/utilisateur" ,u ).pipe()

 }

 getuserById(id: number): Observable<any>
 {
   return this.http.get(this.baseUrl  + id);
 }

 supprimer(id: number): Observable<any>
 {
   return this.http.delete(this.baseUrl +  "/" +id, { responseType: 'text' });
 }
  update(us:utilisateur): any
  {
    return this.http.put(this.baseUrl ,us).pipe();
  }

  getByRole():Observable<utilisateur[]>{
    return this.http.get<utilisateur[]>(this.baseUrl+"/findByRoleName/ABONNE");
  }



}
