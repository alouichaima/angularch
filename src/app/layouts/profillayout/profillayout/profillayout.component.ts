import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from 'src/app/service/token-storage.service';

@Component({
  selector: 'app-profillayout',
  templateUrl: './profillayout.component.html',
  styleUrls: ['./profillayout.component.css']
})
export class ProfillayoutComponent implements OnInit {
  
  private roles : string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  prenom!: string;
  email!: string;
  nom!:string
  
  constructor(private tokenStorageService: TokenStorageService , private route:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;
      this.showAdminBoard = this.roles.includes('ADMIN');
      this.nom= user.nom;
      this.prenom = user.prenom;
      this.email = user.email;

    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
    this.route.navigate(['/login']);
  }

}
