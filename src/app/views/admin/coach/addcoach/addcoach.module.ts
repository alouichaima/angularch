import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddcoachRoutingModule } from './addcoach-routing.module';
import { AddcoachComponent } from './addcoach.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddcoachComponent
  ],
  imports: [
    CommonModule,
    AddcoachRoutingModule,
    FormsModule

  ]
})
export class AddcoachModule { }
