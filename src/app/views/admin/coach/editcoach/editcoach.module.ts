import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditcoachRoutingModule } from './editcoach-routing.module';
import { EditcoachComponent } from './editcoach.component';


@NgModule({
  declarations: [
    EditcoachComponent
  ],
  imports: [
    CommonModule,
    EditcoachRoutingModule,
    FormsModule
  ]
})
export class EditcoachModule { }
