import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListetarifRoutingModule } from './listetarif-routing.module';
import { ListetarifComponent } from './listetarif.component';


@NgModule({
  declarations: [
    ListetarifComponent
  ],
  imports: [
    CommonModule,
    ListetarifRoutingModule
  ]
})
export class ListetarifModule { }
