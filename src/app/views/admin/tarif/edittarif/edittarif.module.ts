import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EdittarifRoutingModule } from './edittarif-routing.module';
import { EdittarifComponent } from './edittarif.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    EdittarifComponent
  ],
  imports: [
    CommonModule,
    EdittarifRoutingModule,
    FormsModule
  ]
})
export class EdittarifModule { }
