import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddespaceRoutingModule } from './addespace-routing.module';
import { AddespaceComponent } from './addespace.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AddespaceComponent
  ],
  imports: [
    CommonModule,
    AddespaceRoutingModule,
    FormsModule


  ]
})
export class AddespaceModule { }
