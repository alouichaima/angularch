import { EspaceService } from './../../../../service/espace.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-editespace',
  templateUrl: './editespace.component.html',
  styleUrls: ['./editespace.component.css']
})
export class EditespaceComponent implements OnInit {
  id:number =0;
  esp:any={'nom':'','description':'','image':''};

  constructor(private service:EspaceService, private router:Router ,private route: ActivatedRoute ) {
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.esp=JSON.parse(params.special);



      }

      })
   }
  ngOnInit(): void {


  }
  onSubmit(){

  }

  retour():void{

    this.router.navigate(['admin/listespace']);

  }
  modif():void{
    this.service.update(this.esp).subscribe({

      next: (data:any)=>{
        this.router.navigate (['admin/listespace'])

     },

     error: (e:any)=> console.error(e),

     complete:()=>{}

     })

  }

}
