import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCRUDService } from 'src/app/service/user-crud.service';

@Component({
  selector: 'app-editmonprofil',
  templateUrl: './editmonprofil.component.html',
  styleUrls: ['./editmonprofil.component.css']
})
export class EditmonprofilComponent implements OnInit {
  id:number =0;
  us: any={'nom':'','prenom':'', 'datenaiss':'', 'email':'', 'telephone':'', 'poids':''};
  constructor(private serviceuser:UserCRUDService,private router:Router,private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{

      if(params&&params.special){

      this.us=JSON.parse(params.special);
 }


      })
   
  }
  onSubmit(){

  }

  retour():void{

    this.router.navigate(['profil/monprofil']);

  }
  modif():void{
    this.serviceuser.update(this.us).subscribe({

      next: (data:any)=>{
        this.router.navigate (['profil/monprofil'])

     },

     error: (u:any)=> console.error(u),

     complete:()=>{}

     })

  }
  

}
